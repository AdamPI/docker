FROM nvcr.io/nvidia/cuda:9.2-cudnn7-devel-ubuntu18.04
LABEL maintainer="Shirong Bai 2020"

ARG PYTHON_VERSION=3.8
ARG CUDA_TOOLKIT=9.2
WORKDIR /workspace

# https://stackoverflow.com/questions/38002543/apt-get-update-returned-a-non-zero-code-100
RUN apt-get update && apt-get install -y apt-transport-https
RUN apt-get update \
    && apt-get install -y \
    software-properties-common \
    vim \
    htop \
    xclip \
    wget \
    net-tools \
    iproute2 \
    tmux \
    curl \
    git \
    ssh \
    build-essential \
    openssh-server
#RUN rm -rf /var/lib/apt/lists/*

# setup tmux
RUN git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm \
    && wget https://bitbucket.org/AdamPI/docker/raw/91bdfc227d2daba5d3a945ba7d35cbb7e5ea2c0e/etcs/dot_tmux.conf_2020-05-11 -O ~/.tmux.conf

# https://stackoverflow.com/questions/16248775/cmake-not-able-to-find-openssl-library
RUN apt-get update && apt-get install -y libssl-dev
# build and install cmake
# RUN git clone https://github.com/Kitware/CMake.git --branch release --single-branch \
#     && cd CMake \
RUN git clone https://AdamPI@bitbucket.org/AdamPI/cmake.git --branch release --single-branch \
    && cd cmake \
    && git checkout tags/v3.17.0 \
    && ./bootstrap && make && make install \
    && cd .. && rm -rf cmake

RUN curl -o ~/miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && chmod +x ~/miniconda.sh \
    && ~/miniconda.sh -b -p /opt/conda \
    && rm ~/miniconda.sh
ENV PATH /opt/conda/bin:$PATH

# conda adds tsinghua sources
RUN conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/ \
    && conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge  \
    && conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/ \
    && conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/ \
    && conda config --set show_channel_urls yes
RUN conda install -y python=$PYTHON_VERSION \
    cython \
    pytorch \
    torchvision \
    cudatoolkit=$CUDA_TOOLKIT \
    tensorboard \
    && /opt/conda/bin/conda clean -ya
# install nni through source code
# assuming conda is installed and __conda_init__ snippet was added to "~/.bashrc"
# "source ~/.bashrc" will activate the default environment, namely "base"
# so that all python modules installed above are visible
SHELL ["/bin/bash", "-c"]
RUN git clone -b v1.6 https://AdamPI@bitbucket.org/AdamPI/nni.git --single-branch \
    && cd nni \
    && /opt/conda/bin/conda init bash && source ~/.bashrc \
    && (echo "y" && cat) | bash install.sh
SHELL ["/bin/sh", "-c"]

WORKDIR /workspace
RUN chmod -R a+w .
